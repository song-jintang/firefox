%global release_build     1
%global debug_build       0
%global build_with_clang  0
%global build_with_asan   0
%global enable_mozilla_crashreporter 0
%ifarch x86_64 %{ix86}
%global enable_mozilla_crashreporter 1
%endif
%if %{build_with_asan}
%global enable_mozilla_crashreporter 0
%endif
%if 0%{?flatpak}
%global enable_mozilla_crashreporter 0
%endif
%global system_nss        1
%global system_ffi        1
%ifarch armv7hl
%global system_libvpx     1
%else
%global system_libvpx     0
%endif
%global hardened_build    1
%global system_jpeg       1
%global run_tests         0
%global disable_elfhack   1
%global use_bundled_cbindgen  1
%ifarch x86_64 aarch64
%if %{release_build}
%global build_with_pgo    0
%else
%global build_with_pgo    0
%endif
%global pgo_wayland       0
%endif
%global wayland_backend_default 0
%if 0%{?flatpak}
%global wayland_backend_default 0
%global build_with_pgo    0
%endif
%ifarch ppc64 s390x
%global big_endian        1
%endif
%ifarch armv7hl
%define _unpackaged_files_terminate_build 0
%global debug_package %{nil}
%endif
%if 0%{?build_with_pgo}
%global use_xvfb          1
%global build_tests       1
%endif
%if 0%{?run_tests}
%global use_xvfb          1
%global build_tests       1
%endif
%global firefox_app_id  \{ec8030f7-c20a-464f-9b0e-13a3a9e97384\}
%global cairo_version 1.13.1
%global freetype_version 2.1.9
%global libnotify_version 0.7.0
%if %{?system_libvpx}
%global libvpx_version 1.8.2
%endif
%if %{?system_nss}
%global nspr_version 4.21
%global nspr_build_version %{nspr_version}
%global nss_version 3.54
%global nss_build_version %{nss_version}
%endif
%global mozappdir     %{_libdir}/%{name}
%global mozappdirdev  %{_libdir}/%{name}-devel-%{version}
%global langpackdir   %{mozappdir}/langpacks
%global tarballdir    firefox-%{version}
%global official_branding       1
%bcond_without langpacks
%if !%{release_build}
%global pre_tag .npgo
%endif
%if %{build_with_clang}
%global pre_tag .clang
%global build_with_pgo    0
%endif
%if %{build_with_asan}
%global pre_tag .asan
%global build_with_pgo    0
%endif
%if !%{system_nss}
%global nss_tag .nss
%endif
Summary:             Mozilla Firefox Web browser
Name:                firefox
Version:             79.0
Release:             11
URL:                 https://www.mozilla.org/firefox/
License:             MPLv1.1 or GPLv2+ or LGPLv2+
Source0:             https://archive.mozilla.org/pub/firefox/releases/%{version}/source/firefox-%{version}.source.tar.xz
%if %{with langpacks}
Source1:             firefox-langpacks-%{version}-20200727.tar.xz
%endif
Source2:             cbindgen-vendor.tar.xz
Source3:             testing.tar.xz
Source10:            firefox-mozconfig
Source12:            firefox.js
Source20:            firefox.desktop
Source21:            firefox.sh.in
Source23:            firefox.1
Source24:            mozilla-api-key
Source25:            firefox-symbolic.svg
Source26:            distribution.ini
Source27:            google-api-key
Source28:            firefox-wayland.sh.in
Source29:            firefox-wayland.desktop
Source30:            firefox-x11.sh.in
Source31:            firefox-x11.desktop
Source32:            node-stdout-nonblocking-wrapper
Source33:            firefox.appdata.xml.in
Source34:            firefox-search-provider.ini
Patch3:              mozilla-build-arm.patch
Patch25:             rhbz-1219542-s390-build.patch
Patch26:             build-icu-big-endian.patch
Patch32:             build-rust-ppc64le.patch
Patch35:             build-ppc-jit.patch
Patch37:             build-jit-atomic-always-lucky.patch
Patch38:             build-cacheFlush-missing.patch
Patch40:             build-aarch64-skia.patch
Patch41:             build-disable-elfhack.patch
Patch44:             build-arm-libopus.patch
Patch46:             firefox-nss-version.patch
Patch48:             build-arm-wasm.patch
Patch49:             build-arm-libaom.patch
Patch51:             mozilla-1640982.patch
Patch215:            firefox-enable-addons.patch
Patch219:            rhbz-1173156.patch
Patch224:            mozilla-1170092.patch
Patch226:            rhbz-1354671.patch
Patch227:            firefox-locale-debug.patch
Patch402:            mozilla-1196777.patch
Patch412:            mozilla-1337988.patch
Patch417:            bug1375074-save-restore-x28.patch
Patch422:            mozilla-1580174-webrtc-popup.patch
Patch575:            firefox-pipewire-0-3.patch
Patch584:            firefox-disable-ffvpx-with-vapi.patch
Patch585:            firefox-vaapi-extra-frames.patch
Patch586:            mozilla-1645671.patch
Patch589:            mozilla-1656436.patch
Patch600:            pgo.patch
Patch601:            mozilla-1516081.patch
Patch602:            mozilla-1516803.patch
Patch603:            CVE-2020-15664.patch
Patch604:            CVE-2020-15665.patch
Patch605:            CVE-2020-15666.patch
Patch606:            CVE-2020-15667.patch
Patch607:            CVE-2020-15668.patch
Patch608:            CVE-2020-15676.patch
Patch609:            CVE-2020-15677.patch
Patch610:            CVE-2020-15678.patch
Patch611:            CVE-2020-15969.patch
Patch612:            CVE-2020-15999.patch
Patch613:            CVE-2020-16012.patch
Patch614:            CVE-2020-26951-1.patch
Patch615:            CVE-2020-26951-2.patch
Patch616:            CVE-2020-26953-pre.patch
Patch617:            CVE-2020-26953.patch
Patch618:            CVE-2020-26956-1.patch
Patch619:            CVE-2020-26956-2.patch
Patch620:            CVE-2020-26956-3.patch
Patch621:            CVE-2020-26957.patch
Patch622:            CVE-2020-26958.patch
Patch623:            CVE-2020-26959.patch
Patch624:            CVE-2020-26960-1.patch
Patch625:            CVE-2020-26960-2.patch
Patch626:            CVE-2020-26961-1.patch
Patch627:            CVE-2020-26961-2.patch
Patch628:            CVE-2020-26963-1.patch
Patch629:            CVE-2020-26963-2.patch
Patch630:            CVE-2020-26965.patch
Patch631:            CVE-2020-26966.patch
Patch632:            CVE-2020-26967.patch
Patch633:            Fix-build-with-rust-nightly.patch
Patch634:            Teach-style_derives-map_type_params-about-mapping-self-correctly.patch
Patch635:            Update-syn-and-proc-macro2-so-that-Firefox-can-build-on-Rust-nightly-again.patch
Patch636:            Bug-1660901-Support-the-fstat-like-subset-of-fstatat-in-the-Linux-sandbox-policies.patch
Patch637:            Bug-1660901-ignore-AT_NO_AUTOMOUNT-in-fstatat-system-call.patch
Patch638:            Bug-1673202-Call-fstat-directly-in-Linux-sandbox-fstatat-interception.patch
Patch639:            Bug-1673770-Extend-the-handling-of-fstatat-as-fstat-to-sandboxes-that-dont-use-a-file-broker.patch
Patch640:            Bug-1680166-Return-EFAULT-when-given-a-null-path-to-stat-calls-in-the-sandbox-filter.patch
Patch641:            Bug-1680166-GCC-is-smarter-than-clang-so-ignore-the-warning-properly.patch
Patch642:            firefox-glibc-dynstack.patch
Patch643:            firefox-crashreporter-build.patch
Patch644:            Deny-clone3-to-force-glibc-fallback.patch

%if %{?system_nss}
BuildRequires:       pkgconfig(nspr) >= %{nspr_version} pkgconfig(nss) >= %{nss_version}
BuildRequires:       nss-static >= %{nss_version}
%endif
BuildRequires:       pkgconfig(libpng)
%if %{?system_jpeg}
BuildRequires:       libjpeg-devel
%endif
BuildRequires:       zip bzip2-devel pkgconfig(zlib) pkgconfig(gtk+-3.0) pkgconfig(gtk+-2.0)
BuildRequires:       pkgconfig(krb5) pkgconfig(pango) pkgconfig(freetype2) >= %{freetype_version}
BuildRequires:       pkgconfig(xt) pkgconfig(xrender) pkgconfig(libstartup-notification-1.0)
BuildRequires:       pkgconfig(libnotify) >= %{libnotify_version} pkgconfig(dri) pkgconfig(libcurl)
BuildRequires:       dbus-glib-devel
%if %{?system_libvpx}
BuildRequires:       libvpx-devel >= %{libvpx_version}
%endif
BuildRequires:       autoconf213 pkgconfig(libpulse) yasm llvm llvm-devel clang clang-libs
%if 0%{?build_with_clang}
BuildRequires:       lld
%endif
BuildRequires:       pipewire-devel
%if !0%{?use_bundled_cbindgen}
BuildRequires:       cbindgen
%endif
BuildRequires:       nodejs nasm >= 1.13 libappstream-glib
%if 0%{?big_endian}
BuildRequires:       icu
%endif
Requires:            mozilla-filesystem p11-kit-trust
%if %{?system_nss}
Requires:            nspr >= %{nspr_build_version} nss >= %{nss_build_version}
%endif
BuildRequires:       python3-devel
%if !0%{?flatpak}
Requires:            u2f-hidraw-policy
%endif
BuildRequires:       nss-devel >= 3.29.1-2.1
Requires:            nss >= 3.48.0
BuildRequires:       desktop-file-utils
%if !0%{?flatpak}
%endif
%if %{?system_ffi}
BuildRequires:       pkgconfig(libffi)
%endif
%if 0%{?use_xvfb}
BuildRequires:       xorg-x11-server-Xvfb
%endif
%if 0%{?pgo_wayland}
BuildRequires:       mutter
%endif
BuildRequires:       rust cargo clang-devel
%if %{build_with_asan}
BuildRequires:       libasan libasan-static
%endif
BuildRequires:       perl-interpreter
Provides:            webclient firefox-wayland = %{version}-%{release}
Obsoletes:           mozilla <= 37:1.7.13 firefox-wayland <= %{version}-%{release}
%description
Mozilla Firefox is an open-source web browser, designed for standards
compliance, performance and portability.
%if %{enable_mozilla_crashreporter}
%global moz_debug_prefix %{_prefix}/lib/debug
%global moz_debug_dir %{moz_debug_prefix}%{mozappdir}
%global uname_m %(uname -m)
%global symbols_file_name %{name}-%{version}.en-US.%{_os}-%{uname_m}.crashreporter-symbols.zip
%global symbols_file_path %{moz_debug_dir}/%{symbols_file_name}
%global _find_debuginfo_opts -p %{symbols_file_path} -o debugcrashreporter.list
%global crashreporter_pkg_name mozilla-crashreporter-%{name}-debuginfo

%package -n %{crashreporter_pkg_name}
Summary:             Debugging symbols used by Mozilla's crash reporter servers
%description -n %{crashreporter_pkg_name}
This package provides debug information for Firefox, for use by
Mozilla's crash reporter servers.  If you are trying to locally
debug %{name}, you want to install %{name}-debuginfo instead.

%files -n %{crashreporter_pkg_name} -f debugcrashreporter.list
%endif
%if 0%{?wayland_backend_default}

%package x11
Summary:             Firefox X11 launcher.
Requires:            %{name}
%description x11
The firefox-x11 package contains launcher and desktop file
to run Firefox explicitly on X11.

%files x11
%{_bindir}/firefox-x11
%{_datadir}/applications/firefox-x11.desktop
%endif
%if %{run_tests}
%global testsuite_pkg_name mozilla-%{name}-testresults

%package -n %{testsuite_pkg_name}
Summary:             Results of testsuite
%description -n %{testsuite_pkg_name}
This package contains results of tests executed during build.

%files -n %{testsuite_pkg_name}
/test_results
%endif

%prep
%setup -q -n %{tarballdir}
tar -xf %{SOURCE3}
%ifarch s390
%patch25 -p1 -b .rhbz-1219542-s390
%endif
%patch40 -p1 -b .aarch64-skia
%if 0%{?disable_elfhack}
%patch41 -p1 -b .disable-elfhack
%endif
%patch3  -p1 -b .arm
%patch44 -p1 -b .build-arm-libopus
%if 0%{?big_endian}
%patch26 -p1 -b .icu
%endif
%patch48 -p1 -b .build-arm-wasm
%patch49 -p1 -b .build-arm-libaom
%patch51 -p1 -b .mozilla-1640982
%patch215 -p1 -b .addons
%patch219 -p1 -b .rhbz-1173156
%patch224 -p1 -b .1170092
%ifarch aarch64
%patch226 -p1 -b .1354671
%endif
%patch227 -p1 -b .locale-debug
%patch402 -p1 -b .1196777
%patch575 -p1 -b .firefox-pipewire-0-3
%patch584 -p1 -b .firefox-disable-ffvpx-with-vapi
%patch585 -p1 -b .firefox-vaapi-extra-frames
%patch586 -p1 -b .mozilla-1645671
%patch589 -p1 -b .mozilla-1656436
%patch600 -p1 -b .pgo
%patch602 -p1 -b .1516803
%patch603 -p1
%patch604 -p1
%patch605 -p1
%patch606 -p1
%patch607 -p1
%patch608 -p1
%patch609 -p1
%patch610 -p1
%patch611 -p1
%patch612 -p1
%patch613 -p1
%patch614 -p1
%patch615 -p1
%patch616 -p1
%patch617 -p1
%patch618 -p1
%patch619 -p1
%patch620 -p1
%patch621 -p1
%patch622 -p1
%patch623 -p1
%patch624 -p1
%patch625 -p1
%patch626 -p1
%patch627 -p1
%patch628 -p1
%patch629 -p1
%patch630 -p1
%patch631 -p1
%patch632 -p1
%patch633 -p1
%patch634 -p1
%patch635 -p1
%patch636 -p1
%patch637 -p1
%patch638 -p1
%patch639 -p1
%patch640 -p1
%patch641 -p1
%patch642 -p1
%patch643 -p1
%patch644 -p1

%{__rm} -f .mozconfig
%{__cp} %{SOURCE10} .mozconfig
echo "ac_add_options --enable-default-toolkit=cairo-gtk3-wayland" >> .mozconfig
%if %{official_branding}
echo "ac_add_options --enable-official-branding" >> .mozconfig
%endif
%{__cp} %{SOURCE24} mozilla-api-key
%{__cp} %{SOURCE27} google-api-key
echo "ac_add_options --prefix=\"%{_prefix}\"" >> .mozconfig
echo "ac_add_options --libdir=\"%{_libdir}\"" >> .mozconfig
%if %{?system_nss}
echo "ac_add_options --with-system-nspr" >> .mozconfig
echo "ac_add_options --with-system-nss" >> .mozconfig
%else
echo "ac_add_options --without-system-nspr" >> .mozconfig
echo "ac_add_options --without-system-nss" >> .mozconfig
%endif
%if %{?system_ffi}
echo "ac_add_options --enable-system-ffi" >> .mozconfig
%endif
%ifarch %{arm}
echo "ac_add_options --disable-elf-hack" >> .mozconfig
%endif
%if %{?debug_build}
echo "ac_add_options --enable-debug" >> .mozconfig
echo "ac_add_options --disable-optimize" >> .mozconfig
%else
%global optimize_flags "none"
%ifarch ppc64le aarch64
%global optimize_flags "-g -O2"
%endif
%if %{optimize_flags} != "none"
echo 'ac_add_options --enable-optimize=%{?optimize_flags}' >> .mozconfig
%else
echo 'ac_add_options --enable-optimize' >> .mozconfig
%endif
echo "ac_add_options --disable-debug" >> .mozconfig
%endif
%ifnarch %{ix86} x86_64
echo "ac_add_options --disable-jemalloc" >> .mozconfig
%endif
%if !%{enable_mozilla_crashreporter}
echo "ac_add_options --disable-crashreporter" >> .mozconfig
%endif
%if 0%{?build_tests}
echo "ac_add_options --enable-tests" >> .mozconfig
%else
echo "ac_add_options --disable-tests" >> .mozconfig
%endif
%if !%{?system_jpeg}
echo "ac_add_options --without-system-jpeg" >> .mozconfig
%else
echo "ac_add_options --with-system-jpeg" >> .mozconfig
%endif
%if %{?system_libvpx}
echo "ac_add_options --with-system-libvpx" >> .mozconfig
%else
echo "ac_add_options --without-system-libvpx" >> .mozconfig
%endif
%ifarch s390 s390x
echo "ac_add_options --disable-ion" >> .mozconfig
%endif
%if %{build_with_asan}
echo "ac_add_options --enable-address-sanitizer" >> .mozconfig
echo "ac_add_options --disable-jemalloc" >> .mozconfig
%endif
echo "ac_add_options --with-mozilla-api-keyfile=`pwd`/mozilla-api-key" >> .mozconfig
echo "ac_add_options --with-google-safebrowsing-api-keyfile=`pwd`/google-api-key" >> .mozconfig
echo 'export NODEJS="%{_buildrootdir}/bin/node-stdout-nonblocking-wrapper"' >> .mozconfig
chmod -x third_party/rust/itertools/src/lib.rs
chmod a-x third_party/rust/gfx-backend-vulkan/src/*.rs
chmod a-x third_party/rust/gfx-hal/src/*.rs
chmod a-x third_party/rust/ash/src/extensions/ext/*.rs
chmod a-x third_party/rust/ash/src/extensions/khr/*.rs

%build
%if 0%{?use_bundled_cbindgen}
mkdir -p my_rust_vendor
cd my_rust_vendor
%{__tar} xf %{SOURCE2}
mkdir -p .cargo
cat > .cargo/config <<EOL
[source.crates-io]
replace-with = "vendored-sources"
[source.vendored-sources]
directory = "`pwd`"
EOL
env CARGO_HOME=.cargo cargo install cbindgen
export PATH=`pwd`/.cargo/bin:$PATH
%endif
cd -
echo "Generate big endian version of config/external/icu/data/icudt67l.dat"
%if 0%{?big_endian}
  icupkg -tb config/external/icu/data/icudt67l.dat config/external/icu/data/icudt67b.dat
  ls -l config/external/icu/data
  rm -f config/external/icu/data/icudt*l.dat
%endif
mkdir %{_buildrootdir}/bin || :
chmod a+x %{SOURCE32}
cp %{SOURCE32} %{_buildrootdir}/bin || :
find ./ -path ./third_party/rust -prune -o -name config.guess -exec cp /usr/lib/rpm/config.guess {} ';'
MOZ_OPT_FLAGS=$(echo "%{optflags}" | %{__sed} -e 's/-Wall//')
MOZ_OPT_FLAGS=$(echo "$MOZ_OPT_FLAGS" | %{__sed} -e 's/-Werror=format-security//')
MOZ_OPT_FLAGS="$MOZ_OPT_FLAGS -fpermissive"
%if %{?hardened_build}
MOZ_OPT_FLAGS="$MOZ_OPT_FLAGS -fPIC -Wl,-z,relro -Wl,-z,now"
%endif
%if %{?debug_build}
MOZ_OPT_FLAGS=$(echo "$MOZ_OPT_FLAGS" | %{__sed} -e 's/-O2//')
%endif
%ifarch s390
MOZ_OPT_FLAGS=$(echo "$MOZ_OPT_FLAGS" | %{__sed} -e 's/-g/-g1/')
export MOZ_DEBUG_FLAGS=" "
%endif
%ifarch %{arm} %{ix86}
MOZ_OPT_FLAGS=$(echo "$MOZ_OPT_FLAGS" | %{__sed} -e 's/-g/-g0/')
export MOZ_DEBUG_FLAGS=" "
%endif
%if !0%{?build_with_clang}
%ifarch s390 ppc aarch64 %{ix86}
MOZ_LINK_FLAGS="-Wl,--no-keep-memory -Wl,--reduce-memory-overheads"
%endif
%ifarch %{arm}
MOZ_LINK_FLAGS="-Wl,--no-keep-memory -Wl,--strip-debug"
echo "ac_add_options --enable-linker=gold" >> .mozconfig
%endif
%endif
%if 0%{?flatpak}
MOZ_LINK_FLAGS="$MOZ_LINK_FLAGS -L%{_libdir}"
%endif
%ifarch x86_64 aarch64
export RUSTFLAGS="-C debuginfo=1"
%endif
%if %{build_with_asan}
MOZ_OPT_FLAGS="$MOZ_OPT_FLAGS -fsanitize=address -Dxmalloc=myxmalloc"
MOZ_LINK_FLAGS="$MOZ_LINK_FLAGS -fsanitize=address -ldl"
%endif
MOZ_OPT_FLAGS="$MOZ_OPT_FLAGS -DNSS_PKCS11_3_0_STRICT"
%if !%{build_with_clang}
echo "export CFLAGS=\"$MOZ_OPT_FLAGS\"" >> .mozconfig
echo "export CXXFLAGS=\"$MOZ_OPT_FLAGS\"" >> .mozconfig
echo "export LDFLAGS=\"$MOZ_LINK_FLAGS\"" >> .mozconfig
%endif
%if 0%{?build_with_clang}
echo "export LLVM_PROFDATA=\"llvm-profdata\"" >> .mozconfig
echo "export AR=\"llvm-ar\"" >> .mozconfig
echo "export NM=\"llvm-nm\"" >> .mozconfig
echo "export RANLIB=\"llvm-ranlib\"" >> .mozconfig
echo "ac_add_options --enable-linker=lld" >> .mozconfig
%else
echo "export CC=gcc" >> .mozconfig
echo "export CXX=g++" >> .mozconfig
echo "export AR=\"gcc-ar\"" >> .mozconfig
echo "export NM=\"gcc-nm\"" >> .mozconfig
echo "export RANLIB=\"gcc-ranlib\"" >> .mozconfig
%endif
%if 0%{?build_with_pgo}
echo "ac_add_options MOZ_PGO=1" >> .mozconfig
echo "ac_add_options --enable-lto" >> .mozconfig
%endif
MOZ_SMP_FLAGS=-j1
%ifarch %{ix86}
[ -z "$RPM_BUILD_NCPUS" ] && \
     RPM_BUILD_NCPUS="`/usr/bin/getconf _NPROCESSORS_ONLN`"
[ "$RPM_BUILD_NCPUS" -ge 2 ] && MOZ_SMP_FLAGS=-j2
%endif
%ifarch x86_64 ppc ppc64 ppc64le aarch64
[ -z "$RPM_BUILD_NCPUS" ] && \
     RPM_BUILD_NCPUS="`/usr/bin/getconf _NPROCESSORS_ONLN`"
[ "$RPM_BUILD_NCPUS" -ge 2 ] && MOZ_SMP_FLAGS=-j2
[ "$RPM_BUILD_NCPUS" -ge 4 ] && MOZ_SMP_FLAGS=-j4
[ "$RPM_BUILD_NCPUS" -ge 8 ] && MOZ_SMP_FLAGS=-j8
%endif
echo "export MOZ_MAKE_FLAGS=\"$MOZ_SMP_FLAGS\"" >> .mozconfig
echo "export MOZ_SERVICES_SYNC=1" >> .mozconfig
echo "export STRIP=/bin/true" >> .mozconfig
%if 0%{?build_with_pgo}
%if 0%{?pgo_wayland}
xvfb-run mutter --wayland --nested &
if [ -z "$WAYLAND_DISPLAY" ]; then
  export WAYLAND_DISPLAY=wayland-0
else
  export WAYLAND_DISPLAY=wayland-1
fi
MOZ_ENABLE_WAYLAND=1 ./mach build  2>&1 | cat -
%else
GDK_BACKEND=x11 xvfb-run ./mach build  2>&1 | cat -
%endif
%else
./mach build -v 2>&1 | cat -
%endif
%if %{enable_mozilla_crashreporter}
make -C objdir buildsymbols
%endif
%if %{?run_tests}
%if %{?system_nss}
ln -s %{_prefix}/bin/certutil objdir/dist/bin/certutil
ln -s %{_prefix}/bin/pk12util objdir/dist/bin/pk12util
%endif
mkdir test_results
./mach --log-no-times check-spidermonkey &> test_results/check-spidermonkey || true
./mach --log-no-times check-spidermonkey &> test_results/check-spidermonkey-2nd-run || true
./mach --log-no-times cppunittest &> test_results/cppunittest || true
xvfb-run ./mach --log-no-times crashtest &> test_results/crashtest || true
./mach --log-no-times gtest &> test_results/gtest || true
xvfb-run ./mach --log-no-times jetpack-test &> test_results/jetpack-test || true
xvfb-run ./mach --log-no-times mochitest-a11y &> test_results/mochitest-a11y || true
xvfb-run ./mach --log-no-times mochitest-browser &> test_results/mochitest-browser || true
xvfb-run ./mach --log-no-times mochitest-chrome &> test_results/mochitest-chrome || true
xvfb-run ./mach --log-no-times mochitest-devtools &> test_results/mochitest-devtools || true
xvfb-run ./mach --log-no-times mochitest-plain &> test_results/mochitest-plain || true
xvfb-run ./mach --log-no-times reftest &> test_results/reftest || true
xvfb-run ./mach --log-no-times webapprt-test-chrome &> test_results/webapprt-test-chrome || true
xvfb-run ./mach --log-no-times webapprt-test-content &> test_results/webapprt-test-content || true
./mach --log-no-times webidl-parser-test &> test_results/webidl-parser-test || true
xvfb-run ./mach --log-no-times xpcshell-test &> test_results/xpcshell-test || true
%if %{?system_nss}
rm -f  objdir/dist/bin/certutil
rm -f  objdir/dist/bin/pk12util
%endif
%endif

%install
%if !0%{?flatpak}
%endif
%{__cat} > objdir/dist/bin/browser/defaults/preferences/firefox-l10n.js << EOF
pref("general.useragent.locale", "chrome://global/locale/intl.properties");
EOF
DESTDIR=%{buildroot} make -C objdir install
%{__mkdir_p} %{buildroot}{%{_libdir},%{_bindir},%{_datadir}/applications}
desktop-file-install --dir %{buildroot}%{_datadir}/applications %{SOURCE20}
%if 0%{?wayland_backend_default}
desktop-file-install --dir %{buildroot}%{_datadir}/applications %{SOURCE31}
%endif
desktop-file-install --dir %{buildroot}%{_datadir}/applications %{SOURCE29}
%if 0%{?wayland_backend_default}
%global wayland_default true
%else
%global wayland_default false
%endif
%{__rm} -rf %{buildroot}%{_bindir}/firefox
%{__sed} -e 's/__DEFAULT_WAYLAND__/%{wayland_default}/' \
         -e 's,/__PREFIX__,%{_prefix},g' %{SOURCE21} > %{buildroot}%{_bindir}/firefox
%{__chmod} 755 %{buildroot}%{_bindir}/firefox
%if 0%{?flatpak}
sed -i -e 's|%FLATPAK_ENV_VARS%|export TMPDIR="$XDG_CACHE_HOME/tmp"|' %{buildroot}%{_bindir}/firefox
%else
sed -i -e 's|%FLATPAK_ENV_VARS%||' %{buildroot}%{_bindir}/firefox
%endif
%if 0%{?wayland_backend_default}
%{__sed} -e 's,/__PREFIX__,%{_prefix},g' %{SOURCE30} > %{buildroot}%{_bindir}/firefox-x11
%{__chmod} 755 %{buildroot}%{_bindir}/firefox-x11
%endif
%{__sed} -e 's,/__PREFIX__,%{_prefix},g' %{SOURCE28} > %{buildroot}%{_bindir}/firefox-wayland
%{__chmod} 755 %{buildroot}%{_bindir}/firefox-wayland
%{__install} -p -D -m 644 %{SOURCE23} %{buildroot}%{_mandir}/man1/firefox.1
%{__rm} -f %{buildroot}/%{mozappdir}/firefox-config
%{__rm} -f %{buildroot}/%{mozappdir}/update-settings.ini
for s in 16 22 24 32 48 256; do
    %{__mkdir_p} %{buildroot}%{_datadir}/icons/hicolor/${s}x${s}/apps
    %{__cp} -p browser/branding/official/default${s}.png \
               %{buildroot}%{_datadir}/icons/hicolor/${s}x${s}/apps/firefox.png
done
%{__mkdir_p} %{buildroot}%{_datadir}/icons/hicolor/symbolic/apps
%{__cp} -p %{SOURCE25} \
           %{buildroot}%{_datadir}/icons/hicolor/symbolic/apps
echo > %{name}.lang
%if %{with langpacks}
%{__mkdir_p} %{buildroot}%{langpackdir}
%{__tar} xf %{SOURCE1}
for langpack in `ls firefox-langpacks/*.xpi`; do
  language=`basename $langpack .xpi`
  extensionID=langpack-$language@firefox.mozilla.org
  %{__mkdir_p} $extensionID
  unzip -qq $langpack -d $extensionID
  find $extensionID -type f | xargs chmod 644
  cd $extensionID
  zip -qq -r9mX ../${extensionID}.xpi *
  cd -
  %{__install} -m 644 ${extensionID}.xpi %{buildroot}%{langpackdir}
  language=`echo $language | sed -e 's/-/_/g'`
%if 0%{?flatpak}
  echo "%{langpackdir}/${extensionID}.xpi" >> %{name}.lang
%else
  echo "%%lang($language) %{langpackdir}/${extensionID}.xpi" >> %{name}.lang
%endif
done
%{__rm} -rf firefox-langpacks
function create_default_langpack() {
language_long=$1
language_short=$2
cd %{buildroot}%{langpackdir}
ln -s langpack-$language_long@firefox.mozilla.org.xpi langpack-$language_short@firefox.mozilla.org.xpi
cd -
echo "%%lang($language_short) %{langpackdir}/langpack-$language_short@firefox.mozilla.org.xpi" >> %{name}.lang
}
create_default_langpack "es-AR" "es"
create_default_langpack "fy-NL" "fy"
create_default_langpack "ga-IE" "ga"
create_default_langpack "gu-IN" "gu"
create_default_langpack "hi-IN" "hi"
create_default_langpack "hy-AM" "hy"
create_default_langpack "nb-NO" "nb"
create_default_langpack "nn-NO" "nn"
create_default_langpack "pa-IN" "pa"
create_default_langpack "pt-PT" "pt"
create_default_langpack "sv-SE" "sv"
create_default_langpack "zh-TW" "zh"
%endif
%{__mkdir_p} %{buildroot}/%{mozappdir}/browser/defaults/preferences
%{__mkdir_p} %{buildroot}/%{_sysconfdir}/%{name}/pref
%{__mkdir_p} %{buildroot}%{_datadir}/mozilla/extensions/%{firefox_app_id}
%{__mkdir_p} %{buildroot}%{_libdir}/mozilla/extensions/%{firefox_app_id}
%{__install} -p -c -m 644 LICENSE %{buildroot}/%{mozappdir}
%{__rm} -rf %{buildroot}%{mozappdir}/dictionaries
ln -s %{_datadir}/myspell %{buildroot}%{mozappdir}/dictionaries
%if %{enable_mozilla_crashreporter}
sed -i -e "s/\[Crash Reporter\]/[Crash Reporter]\nEnabled=1/" %{buildroot}/%{mozappdir}/application.ini
%{__mkdir_p} %{buildroot}/%{moz_debug_dir}
%{__cp} objdir/dist/%{symbols_file_name} %{buildroot}/%{moz_debug_dir}
%endif
%if %{run_tests}
%{__mkdir_p} %{buildroot}/test_results
%{__cp} test_results/* %{buildroot}/test_results
%endif
%{__cp} %{SOURCE12} %{buildroot}%{mozappdir}/browser/defaults/preferences
%{__cp} build/unix/run-mozilla.sh %{buildroot}%{mozappdir}
%{__mkdir_p} %{buildroot}%{mozappdir}/distribution
%{__cp} %{SOURCE26} %{buildroot}%{mozappdir}/distribution
mkdir -p %{buildroot}%{_datadir}/metainfo
%{__sed} -e 's/__VERSION__/%{version}/' %{SOURCE33} > %{buildroot}%{_datadir}/metainfo/firefox.appdata.xml
mkdir -p %{buildroot}%{_datadir}/gnome-shell/search-providers
%{__cp} %{SOURCE34} %{buildroot}%{_datadir}/gnome-shell/search-providers
rm -f %{buildroot}%{mozappdirdev}/sdk/lib/libmozjs.so
rm -f %{buildroot}%{mozappdirdev}/sdk/lib/libmozalloc.so
rm -f %{buildroot}%{mozappdirdev}/sdk/lib/libxul.so

%pretrans -p <lua>
require 'posix'
require 'os'
if (posix.stat("%{mozappdir}/browser/defaults/preferences", "type") == "link") then
  posix.unlink("%{mozappdir}/browser/defaults/preferences")
  posix.mkdir("%{mozappdir}/browser/defaults/preferences")
  if (posix.stat("%{mozappdir}/defaults/preferences", "type") == "directory") then
    for i,filename in pairs(posix.dir("%{mozappdir}/defaults/preferences")) do
      os.rename("%{mozappdir}/defaults/preferences/"..filename, "%{mozappdir}/browser/defaults/preferences/"..filename)
    end
    f = io.open("%{mozappdir}/defaults/preferences/README","w")
    if f then
      f:write("Content of this directory has been moved to %{mozappdir}/browser/defaults/preferences.")
      f:close()
    end
  end
end

%check
appstream-util validate-relax --nonet %{buildroot}%{_datadir}/metainfo/*.appdata.xml

%preun
if [ $1 -eq 0 ]; then
  %{__rm} -rf %{mozappdir}/components
  %{__rm} -rf %{mozappdir}/extensions
  %{__rm} -rf %{mozappdir}/plugins
  #%{__rm} -rf %{langpackdir}
fi

%post
update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
update-desktop-database &> /dev/null || :
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%{_bindir}/firefox
%{_bindir}/firefox-wayland
%{mozappdir}/firefox
%{mozappdir}/firefox-bin
%doc %{_mandir}/man1/*
%dir %{_sysconfdir}/%{name}
%dir %{_sysconfdir}/%{name}/*
%dir %{_datadir}/mozilla/extensions/*
%dir %{_libdir}/mozilla/extensions/*
%{_datadir}/applications/%{name}.desktop
%{_datadir}/metainfo/*.appdata.xml
%{_datadir}/gnome-shell/search-providers/*.ini
%dir %{mozappdir}
%license %{mozappdir}/LICENSE
%{mozappdir}/browser/chrome
%{mozappdir}/browser/defaults/preferences/firefox.js
%{mozappdir}/browser/features/*.xpi
%{mozappdir}/distribution/distribution.ini
%ghost %{mozappdir}/browser/features/aushelper@mozilla.org.xpi
%if %{with langpacks}
%dir %{langpackdir}
%endif
%{mozappdir}/browser/omni.ja
%{mozappdir}/run-mozilla.sh
%{mozappdir}/application.ini
%{mozappdir}/pingsender
%exclude %{mozappdir}/removed-files
%{_datadir}/icons/hicolor/16x16/apps/firefox.png
%{_datadir}/icons/hicolor/22x22/apps/firefox.png
%{_datadir}/icons/hicolor/24x24/apps/firefox.png
%{_datadir}/icons/hicolor/256x256/apps/firefox.png
%{_datadir}/icons/hicolor/32x32/apps/firefox.png
%{_datadir}/icons/hicolor/48x48/apps/firefox.png
%{_datadir}/icons/hicolor/symbolic/apps/firefox-symbolic.svg
%{_datadir}/applications/firefox-wayland.desktop
%if %{enable_mozilla_crashreporter}
%{mozappdir}/crashreporter
%{mozappdir}/crashreporter.ini
%{mozappdir}/minidump-analyzer
%{mozappdir}/Throbber-small.gif
%{mozappdir}/browser/crashreporter-override.ini
%endif
%{mozappdir}/*.so
%{mozappdir}/gtk2/*.so
%{mozappdir}/defaults/pref/channel-prefs.js
%{mozappdir}/dependentlibs.list
%{mozappdir}/dictionaries
%{mozappdir}/omni.ja
%{mozappdir}/platform.ini
%{mozappdir}/plugin-container
%{mozappdir}/gmp-clearkey
%{mozappdir}/fonts/TwemojiMozilla.ttf
%if !%{?system_nss}
%exclude %{mozappdir}/libnssckbi.so
%endif
%if %{build_with_asan}
%{mozappdir}/llvm-symbolizer
%endif

%changelog
* Thu Oct 14 2021 zhangweiguo <zhangweiguo2@huawei.com> - 79.0-11
- Set RUSTFLAGS:debuginfo to 1

* Mon Aug 23 2021 lingsheng <lingsheng@huawei.com> - 79.0-10
- Fix firefox tab crash with clone3 in glibc 2.34

* Wed Aug 11 2021 caodongxia <caodongxia@huawei.com> - 79.0-9
- Fix glibc dynstack 

* Wed Jul  7 2021 lingsheng <lingsheng@huawei.com> - 79.0-8
- Fix firefox video tab crash with rust 1.51

* Mon Jul 05 2021 caodongxia <caodongxia@huawei.com> - 79.0-7
- Fix build with pipewire 0.3

* Fri May 14 2021 lingsheng <lingsheng@huawei.com> - 79.0-6
- Fix build with rust 1.51

* Thu Jan 07 2021 wangxiao <wangxiao65@huawei.com> - 79.0-5
- Fix CVE-2020-15969 CVE-2020-15999 CVE-2020-16012 CVE-2020-26951
  CVE-2020-26953 CVE-2020-26956 CVE-2020-26957 CVE-2020-26958
  CVE-2020-26959 CVE-2020-26960 CVE-2020-26961 CVE-2020-26963
  CVE-2020-26965 CVE-2020-25966 CVE-2020-26967

* Wed Dec 16 2020 wangxiao <wangxiao65@huawei.com> - 79.0-4
- Fix CVE-2020-15664 CVE-2020-15665 CVE-2020-15666 CVE-2020-15667
  CVE-2020-15668 CVE-2020-15676 CVE-2020-15677 CVE-2020-15678

* Thu Nov 28 2020 Jeffery.Gao <gaojianxing@huawei.com> - 79.0-3
- Fix firefox downgrade error

* Mon Oct 26 2020 lingsheng <lingsheng@huawei.com> - 79.0-2
- Fix firefox remove warning

* Mon Aug 17 2020 zhangjiapeng <zhangjiapeng9@huawei.com> - 79.0-1
- Update to 79.0-1

* Wed Jul 22 2020 lingsheng <lingsheng@huawei.com> - 62.0.3-7
- Fix firefox build with rust 1.38

* Mon May 25 2020 huanghaitao <huanghaitao8@huawei.com> - 62.0.3-6
- Type:cves
- ID: CVE-2020-6811 CVE-2020-6814
- SUG:restart
- DESC: fix CVE-2020-6811 CVE-2020-6814

* Mon May 25 2020 Captain Wei<captain.a.wei@gmail.com> - 62.0.3-5
- fix unstable_name_collisions problem when compile

* Wen Feb 26 2020 xuxijian<xuxijian@huawei.com> - 62.0.3-4
- Package init
