
# HG changeset patch
# User Emilio Cobos Álvarez <emilio@crisal.io>
# Date 1606907116 0
# Node ID ff436849850a87dfa3db032e80eaba5e5a3536f4
# Parent  f326cd8f9f19a8f3b5edf9d6838fdce5b02d9c64
Bug 1680166 - Return EFAULT when given a null path to stat* calls in the sandbox filter. r=gcp, a=RyanVM

It's a common way to check the existence of system calls. Glibc may fall
back to fstatat when statx is called, passing down the null path.

Since we handle fstatat, let's return -EFAULT the same way the real
fstatat syscall would do.

This is needed for the sandbox not to constantly crash due to this statx
call in rustc:

https://github.com/rust-lang/rust/blob/09c9c9f7da72b774cc445c0f56fc0b9792a49647/library/std/src/sys/unix/fs.rs#L119-L123

Differential Revision: https://phabricator.services.mozilla.com/D98414

diff --git a/security/sandbox/linux/SandboxBrokerClient.cpp b/security/sandbox/linux/SandboxBrokerClient.cpp
--- a/security/sandbox/linux/SandboxBrokerClient.cpp
+++ b/security/sandbox/linux/SandboxBrokerClient.cpp
@@ -154,21 +154,29 @@ int SandboxBrokerClient::Open(const char
 }
 
 int SandboxBrokerClient::Access(const char* aPath, int aMode) {
   Request req = {SANDBOX_FILE_ACCESS, aMode, 0};
   return DoCall(&req, aPath, nullptr, nullptr, false);
 }
 
 int SandboxBrokerClient::Stat(const char* aPath, statstruct* aStat) {
+  if (!aPath || !aStat) {
+    return -EFAULT;
+  }
+
   Request req = {SANDBOX_FILE_STAT, 0, sizeof(statstruct)};
   return DoCall(&req, aPath, nullptr, (void*)aStat, false);
 }
 
 int SandboxBrokerClient::LStat(const char* aPath, statstruct* aStat) {
+  if (!aPath || !aStat) {
+    return -EFAULT;
+  }
+
   Request req = {SANDBOX_FILE_STAT, O_NOFOLLOW, sizeof(statstruct)};
   return DoCall(&req, aPath, nullptr, (void*)aStat, false);
 }
 
 int SandboxBrokerClient::Chmod(const char* aPath, int aMode) {
   Request req = {SANDBOX_FILE_CHMOD, aMode, 0};
   return DoCall(&req, aPath, nullptr, nullptr, false);
 }
diff --git a/security/sandbox/linux/SandboxFilter.cpp b/security/sandbox/linux/SandboxFilter.cpp
--- a/security/sandbox/linux/SandboxFilter.cpp
+++ b/security/sandbox/linux/SandboxFilter.cpp
@@ -239,30 +239,30 @@ class SandboxPolicyCommon : public Sandb
 
   static intptr_t StatAtTrap(ArgsRef aArgs, void* aux) {
     auto broker = static_cast<SandboxBrokerClient*>(aux);
     auto fd = static_cast<int>(aArgs.args[0]);
     auto path = reinterpret_cast<const char*>(aArgs.args[1]);
     auto buf = reinterpret_cast<statstruct*>(aArgs.args[2]);
     auto flags = static_cast<int>(aArgs.args[3]);
 
-    if (fd != AT_FDCWD && (flags & AT_EMPTY_PATH) != 0 &&
-        strcmp(path, "") == 0) {
+    if (fd != AT_FDCWD && (flags & AT_EMPTY_PATH) && path &&
+        !strcmp(path, "")) {
 #ifdef __NR_fstat64
       return DoSyscall(__NR_fstat64, fd, buf);
 #else
       return DoSyscall(__NR_fstat, fd, buf);
 #endif
     }
 
     if (!broker) {
       return BlockedSyscallTrap(aArgs, nullptr);
     }
 
-    if (fd != AT_FDCWD && path[0] != '/') {
+    if (fd != AT_FDCWD && path && path[0] != '/') {
       SANDBOX_LOG_ERROR("unsupported fd-relative fstatat(%d, \"%s\", %p, %d)",
                         fd, path, buf, flags);
       return BlockedSyscallTrap(aArgs, nullptr);
     }
     if ((flags & ~(AT_SYMLINK_NOFOLLOW | AT_NO_AUTOMOUNT)) != 0) {
       SANDBOX_LOG_ERROR("unsupported flags %d in fstatat(%d, \"%s\", %p, %d)",
                         (flags & ~(AT_SYMLINK_NOFOLLOW | AT_NO_AUTOMOUNT)), fd,
                         path, buf, flags);
diff --git a/security/sandbox/linux/gtest/TestBroker.cpp b/security/sandbox/linux/gtest/TestBroker.cpp
--- a/security/sandbox/linux/gtest/TestBroker.cpp
+++ b/security/sandbox/linux/gtest/TestBroker.cpp
@@ -212,16 +212,24 @@ TEST_F(SandboxBrokerTest, Access) {
 
 TEST_F(SandboxBrokerTest, Stat) {
   statstruct realStat, brokeredStat;
   ASSERT_EQ(0, statsyscall("/dev/null", &realStat)) << "Shouldn't ever fail!";
   EXPECT_EQ(0, Stat("/dev/null", &brokeredStat));
   EXPECT_EQ(realStat.st_ino, brokeredStat.st_ino);
   EXPECT_EQ(realStat.st_rdev, brokeredStat.st_rdev);
 
+  // Add some indirection to avoid -Wnonnull warnings.
+  [&](const char* aPath) {
+    EXPECT_EQ(-1, statsyscall(aPath, &realStat));
+    EXPECT_EQ(errno, EFAULT);
+
+    EXPECT_EQ(-EFAULT, Stat(aPath, &brokeredStat));
+  }(nullptr);
+
   EXPECT_EQ(-ENOENT, Stat("/var/empty/qwertyuiop", &brokeredStat));
   EXPECT_EQ(-EACCES, Stat("/dev", &brokeredStat));
 
   EXPECT_EQ(0, Stat("/proc/self", &brokeredStat));
   EXPECT_TRUE(S_ISDIR(brokeredStat.st_mode));
 }
 
 TEST_F(SandboxBrokerTest, LStat) {

